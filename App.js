import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import Router from './src/navigation/Router';
import DestinationSearchScreen from './src/screens/DestinationSearch/DestinationSearchScreen';
import HomeScreen from './src/screens/Home/Home';

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Router />
    </>
  );
};

export default App;
