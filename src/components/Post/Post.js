import React from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles';

const PostScreen = ({post}) => {
  return (
    <>
      <View style={styles.container}>
        {/* Image */}

        <Image
          style={styles.image}
          source={{
            uri: post.image,
          }}
        />

        {/* Bed and bedroom */}

        <Text style={styles.bedrooms}>
          {post.bed} bed - {post.bedroom} bedrooms
        </Text>

        {/* Type & Description */}
        <Text style={styles.description} numberOfLines={2}>
          {post.type}. {post.title}
        </Text>

        {/* old price and new price */}
        <Text style={styles.prices}>
          <Text style={styles.oldPrice}>${post.oldPrice} </Text>
          <Text style={styles.price}> ${post.newPrice}</Text> / night
        </Text>

        {/* total price */}
        <Text style={styles.totalPrice}>${post.totalPrice} total</Text>
      </View>
    </>
  );
};

export default PostScreen;
