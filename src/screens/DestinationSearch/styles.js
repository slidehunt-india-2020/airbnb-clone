import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  textInput: {
    fontSize: 18,
    marginBottom: 20,
  },
  container: {
    margin: 20,
  },
  locationText: {},
  iconContainer: {
    backgroundColor: '#d4d4d4',
    padding: 5,
    borderRadius: 10,
    marginRight: 15,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 7,
    borderBottomWidth: 1,
    borderColor: 'lightgrey',
    paddingVertical: 15,
  },
});

export default styles;
