import React from 'react';
import {FlatList, Text, TextInput, View} from 'react-native';
import styles from './styles';
import searchResults from '../../../assets/data/search';

import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';

const DestinationSearchScreen = () => {
  const [inputText, setInputText] = React.useState('');

  return (
    <>
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          placeholder="Where are you going?"
          value={inputText}
          onChangeText={setInputText}
        />

        <FlatList
          data={searchResults}
          renderItem={({item}) => (
            <View style={styles.row}>
              <View style={styles.iconContainer}>
                <Entypo name={'location-pin'} size={30} />
              </View>
              <Text style={styles.locationText}>{item.description}</Text>
            </View>
          )}
        />
      </View>
    </>
  );
};

export default DestinationSearchScreen;
