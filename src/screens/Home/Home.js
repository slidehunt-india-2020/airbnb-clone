import React from 'react';
import {ImageBackground, Pressable, Text, View} from 'react-native';

import styles from './styles';
import PostScreen from '../../components/Post/Post';
import Fontisto from 'react-native-vector-icons/Fontisto';
import feed from '../../../assets/data/feed';
import SearchPage from '../SearchResults/Search';
import {useNavigation} from '@react-navigation/core';

const post1 = feed[0];

const HomeScreen = () => {
  const navigation = useNavigation();
  return (
    <>
      <View>
        {/* Search card  */}

        {/* <PostScreen post={post1} /> */}

        {/* <SearchPage /> */}

        <ImageBackground
          source={require('../../../assets/images/wallpaper.jpg')}
          style={styles.image}>
          <Pressable
            style={styles.searchButton}
            onPress={() => navigation.navigate('Destination Search')}>
            <Fontisto name="search" size={25} color={'#f15454'} />
            <Text style={styles.searchButtonText}>Where are you going ?</Text>
          </Pressable>
          <Text style={styles.title}>Go Near</Text>

          <Pressable
            style={styles.button}
            onPress={() => console.warn('Explore btn clicked! ')}>
            <Text style={styles.buttonText}>Explore nearby stays</Text>
          </Pressable>
        </ImageBackground>
      </View>
    </>
  );
};

export default HomeScreen;
