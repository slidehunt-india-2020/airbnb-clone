import React from 'react';
import {FlatList, View} from 'react-native';
import feed from '../../../assets/data/feed';
import PostScreen from '../../components/Post/Post';

const SearchPage = () => {
  return (
    <>
      <View>
        <FlatList
          data={feed}
          renderItem={({item}) => <PostScreen post={item} />}
        />
      </View>
    </>
  );
};

export default SearchPage;
